async function createSandboxMacro(data, slot) {
    if (data.type != "rollable") return;
    let rollData = data.data;

    // Add quotes to the strings if they are NOT null/true/false
    rollData.attrID = await placeholderParser(rollData.attrID);
    rollData.attKey = await placeholderParser(rollData.attKey);
    rollData.citemID = await placeholderParser(rollData.citemID);
    rollData.citemKey = await placeholderParser(rollData.citemKey);
    rollData.tableKey = await placeholderParser(rollData.tableKey);

    // Do not put whitespace above the start of the macro,
    // or else (command === m.data.command) is always false
    const command =
        `let rollData = {
    attrID: ${rollData.attrID},
    attKey: ${rollData.attKey},
    citemID: ${rollData.citemID},
    citemKey: ${rollData.citemKey},
    ciRoll: ${rollData.ciRoll},
    isFree: ${rollData.isFree},
    tableKey: ${rollData.tableKey},
    useData: ${rollData.useData}
};

// This is the ID the macro was dragged from
// For checking Free Table ID's
let originalActorId = "${data.actorId}";

const speaker = ChatMessage.getSpeaker();
let actor;
if (speaker.token) actor = game.actors.tokens[speaker.token];
if (!actor) actor = game.actors.get(speaker.actor);
if (actor) {
    let letsContinue = true;
    // Check if actor possess the citem
    if(rollData.citemKey != null)
        if(!actor.data.data.citems.find(ci => ci.ciKey === rollData.citemKey))
            return ui.notifications.warn("Current actor does not possess the required citem.");

    // Check if the free table item still exists
    if(rollData.isFree)
        if(!actor.data.data.attributes[rollData.tableKey].tableitems.find(ti => ti.id === rollData.citemID))
            return ui.notifications.warn("Current actor does not possess the referenced Free Table id.");
        // Check if the selected actor is the original
        else if (actor.data._id != originalActorId)
            await Dialog.confirm({
                title: "ARE YOU SURE ABOUT THAT?",
                content: "You are about to roll a Free Table id that isn't from the same actor you created the macro from.<br><br>This may have unintended results due to targetting a different id owned by that actor.",
                yes: () => {},
                no: () => {letsContinue = false;},
                defaultYes: true
            });

        if(letsContinue)
            actor.sheet._onRollCheck(${rollData.attrID}, ${rollData.attKey}, ${rollData.citemID}, ${rollData.citemKey}, ${rollData.ciRoll}, ${rollData.isFree}, ${rollData.tableKey}, ${rollData.useData});
    } else
    ui.notifications.warn("Couldn't find actor. Select a token.");`;

    let actorName = game.actors.get(data.actorId).name;
    if (game.user.isGM)
        actorName = "GM";
    let macroName = "[" + actorName + "] " + rollData.tag;

    let macro = game.macros.contents.find(m => (m.name === macroName) && (m.data.command === command));
    if (!macro) {
        macro = await Macro.create({
            name: macroName,
            type: "script",
            img: rollData.img,
            command: command,
            flags: {},
            permission: game.actors.get(data.actorId).data.permission
        });
        ui.notifications.warn('Macro created.');
    }

    await game.user.assignHotbarMacro(macro, slot);
    return false;
}

// Add quotes to the strings if they are NOT null/true/false
async function placeholderParser(str) {
    if (str != null && str != true && str != false) {
        str = "\"" + str + "\"";
    }
    return str;
}

/* Sets up the data transfer within a drag event. This function is triggered
* when the user starts dragging any rollable element, and dataTransfer is set to the 
* relevant data needed by the _onDrop function.*/
async function _onDragStart(event, data = null, attrID = null, attKey = null, citemID = null, citemKey = null, ciRoll = false, isFree = false, tableKey = null, useData = null) { //ignore these params for now...
    // If lazily calling _onDragStart(event) with no other parameters
    // then assume you want a standard actor property (ID, Key)
    if (!attrID)
        attrID = event.currentTarget.getAttribute("attid");
    if (!attKey)
        attKey = event.currentTarget.getAttribute("id");

    let propertyItem = game.items.get(attrID);
    let tag = propertyItem.data.data.tag;
    // If tag is blank, use the property key instead? could also use the item's name.
    if (tag == "")
        tag = propertyItem.data.data.attKey
    let img = propertyItem.img;

    // Use cItem image and name + property tag
    if (citemID != null && !isFree) {
        let cItem = game.items.get(citemID);
        tag = cItem.name + " " + tag;
        img = cItem.img;
    }

    // Use Group or Table img & name?
    if (isFree) {
        let tableItem = game.items.contents.find(i => i.data.data.attKey === tableKey);
        let groupItem = game.items.get(tableItem.data.data.group.id);
        tag = groupItem.name + " " + tag + " (" + citemID + ")";
        img = groupItem.img;
    }

    event.dataTransfer.setData("text/plain", JSON.stringify({
        type: "rollable",
        actorId: data.actor.data._id,
        data: {
            attrID: attrID,
            attKey: attKey,
            citemID: citemID,
            citemKey: citemKey,
            ciRoll: ciRoll,
            isFree: isFree,
            tableKey: tableKey,
            useData: useData,
            tag: tag,
            img: img
        }
    }));
}

/*====  HOOKS  ===*/
Hooks.on("rendergActorSheet", (app, html, data) => {
    if (data.actor.isOwner) {
        let handler = ev => _onDragStart(ev, data);
        // Find all .rollable on the character sheet.
        html.find('.rollable').each((i, rollable) => {
            // Add draggable attribute and dragstart listener.
            rollable.setAttribute("draggable", true);
            rollable.addEventListener("dragstart", handler, false);
        });
    }
});

Hooks.once('ready', async () => {
    Hooks.on("hotbarDrop", (bar, data, slot) => createSandboxMacro(data, slot));
});
