# sandbox-drag-drop-actor-rolls

Sandbox System Module that allows you to drag and drop Actor Rolls (not citem) to the Macro Hotbar

## How to use?

Download the directory and extract into your foundry's Data\modules folder.

After activating it in your game world, you should be able to drag any actor roll from a character sheet to the macro hot bar.

NOTE: Currently does not work with citem rolls (official implementation may be necessary)

This was tested quite extensively on Sandbox vv11, but it should be compatible with Sandbox v12 as well.

Note, that as a GM you must have a token selected before triggering a macro. I tried to make warning and errors appear as necessary, but they are only in English.

## Version Support

DOES NOT WORK ON FOUNDRY V9.251 AND HIGHER
